<?php
//programa que comprueva si un numero dado es positivo o negativo
//inicializacion de las variables
$numero = 0;
$salida = "";
//recibimos los datos por post	
$numero = $_POST['numero'];

//procesamiento
if ($numero > 0) {
    $salida = "El numero {$numero} es positivo";
} else {
    $salida = "El numero {$numero} es negativo";
}
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Ejercicio 2 salida</title>
</head>

<body>
    <div>
        <?= $salida ?>
    </div>
</body>

</html>