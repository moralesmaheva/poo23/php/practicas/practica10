<?php
//formulario que recoge nombre, color, alto y peso
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Ejercicio 1 formulario</title>
</head>

<body>
    <form action="1salida.php" method="post">
        <div>
            <label for="nombre">Nombre: </label>
            <input type="text" name="nombre" id="nombre" required>
        </div>
        <div>
            <label for="color">Color: </label>
            <input type="text" name="color" id="color" required>
        </div>
        <div>
            <label for="alto">Alto: </label>
            <input type="number" name="alto" id="alto" required>
        </div>
        <div>
            <label for="peso">Peso: </label>
            <input type="number" name="peso" id="peso" required>
        </div>
        <div>
            <button>Enviar</button>
        </div>
    </form>
</body>

</html>