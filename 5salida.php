<?php
// indicar el mayor de tres numeros dados por formulario

//inicializar variables
$numero1 = 0;
$numero2 = 0;
$numero3 = 0;
$salida = "";

//recibimos los datos por post
$numero1 = $_POST["numero1"];
$numero2 = $_POST["numero2"];
$numero3 = $_POST["numero3"];

//procesamiento
if ($numero1 > $numero2 && $numero1 > $numero3) {
    $salida = "El numero {$numero1} es mayor que {$numero2} y {$numero3}";
} else if ($numero2 > $numero1 && $numero2 > $numero3) {
    $salida = "El numero {$numero2} es mayor que {$numero1} y {$numero3}";
} else {
    $salida = "El numero {$numero3} es mayor que {$numero1} y {$numero2}";
}
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Ejercicio 5 salida</title>
</head>

<body>
    <div>
        <?= $salida ?>
    </div>
</body>

</html>