<?php
//formulario que permite introducir un caracter y te indica si es una vocal
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Ejercicio 3 formulario</title>
</head>

<body>
    <form action="3salida.php" method="post">
        <div>
            <label for="caracter">Introduce un caracter </label>
            <input type="text" name="caracter" placeholder="Introduzca un caracter" required>
        </div>
        <div>
            <button>Comprobar</button>
        </div>
    </form>

</body>

</html>