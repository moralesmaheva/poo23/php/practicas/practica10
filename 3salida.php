<?php
//programa que comprueba caracter enviado por formulario y te indica si es vocal
//inicializacion de las variables
$caracter = "";
$vocales = ["a", "e", "i", "o", "u"];
$salida = "";
//recibimos los datos por post
$caracter = $_POST['caracter'];

//procesamiento
if (in_array($caracter, $vocales)) {
    $salida = "{$caracter} es una vocal";
} else {
    $salida = "{$caracter} no es una vocal";
}
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>

<body>
    <div>
        <?= $salida ?>
    </div>
</body>

</html>