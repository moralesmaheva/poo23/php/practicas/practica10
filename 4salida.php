<?php
//comprobar si el numero dado por formulario es par o impar

// inicializamos variables
$numero = 0;
$salida = "";

//recibimos los datos por post
$numero = $_POST['numero'];

//procesamiento
if ($numero % 2 == 0) {
    $salida = "El numero {$numero} es par";
} else {
    $salida = "El numero {$numero} es impar";
}
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Ejercicii 4 salida</title>
</head>

<body>
    <div>
        <?= $salida ?>
    </div>
</body>

</html>