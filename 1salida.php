<?php
// programa que lee el valor de las cajas del formulario y muestra el resultado en una tabla

//inicializo las variables
$nombre = "";
$color = "";
$alto = "";
$peso = "";

//recibo los datos del formulario por post
$nombre = $_POST["nombre"];
$color = $_POST["color"];
$alto = $_POST["alto"];
$peso = $_POST["peso"];
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>

<body>
    <!-- creo la tabla e imprimo dentro los resultados -->
    <table border="1" width="100%">
        <tr>
            <td>Nombre</td>
            <td><?= $nombre ?></td>
        </tr>
        <tr>
            <td>Color</td>
            <td><?= $color ?></td>
        </tr>
        <tr>
            <td>Alto</td>
            <td><?= $alto ?></td>
        </tr>
        <tr>
            <td>Peso</td>
            <td><?= $peso ?></td>
        </tr>
    </table>
</body>

</html>