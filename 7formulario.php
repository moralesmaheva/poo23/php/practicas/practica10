<?php
//formulario que permite introducir dos numeros y devuelva 
//suma, resta, multiplicacion y division
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Ejercicio 7 formulario</title>
</head>

<body>
    <form action="7salida.php" method="post">
        <div>
            <label for="numero1">Primer numero</label>
            <input type="number" name="numero1" id="numero1" placeholder="Introduce el primer numero" required>
        </div>
        <div>
            <label for="numero2">Segundo numero</label>
            <input type="number" name="numero2" id="numero2" placeholder="Introduce el segundo numero" required>
        </div>
        <div>
            <button>Enviar</button>
        </div>
    </form>
</body>

</html>