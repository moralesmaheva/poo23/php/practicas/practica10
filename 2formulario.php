<?php
//formulario que recoge un numero para comprobar si es positivo o negativo
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Ejercicio 2 formulario</title>
</head>

<body>
    <form action="2salida.php" method="post">
        <div>
            <label for="numero">Numero</label>
            <input type="number" name="numero" id="numero" placeholder="Introduce un numero" required>
        </div>
        <div>
            <button>Comprobar</button>
        </div>
    </form>
</body>

</html>