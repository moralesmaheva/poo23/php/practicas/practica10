<?php
//comprobar el intervalo en el que se encuentra la nota y mostrar el mensaje correspondiente

//inicializar variables
$nota = 0;

//recibimos los datos por post
$nota = $_POST["nota"];

//procesamiento

if ($nota < 3) {
    $salida = "Muy deficiente";
} elseif ($nota < 5) {
    $salida = "Suspenso";
} elseif ($nota < 6) {
    $salida = "Suficiente";
} elseif ($nota < 7) {
    $salida = "Bien";
} elseif ($nota < 9) {
    $salida = "Notable";
} else {
    $salida = "Sobresaliente";
}
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Ejercicio 9 salida</title>
</head>

<body>
    <div>
        <?= $salida ?>
    </div>
</body>

</html>