<?php
// formulario que permite introducir el radio de un circulo y calcula
// el area, perimetro y volumen de la esfera
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Ejercicio 8 formulario</title>
</head>

<body>
    <form action="8salida.php" method="post">
        <div>
            <label for="radio">Radio</label>
            <input type="number" name="radio" id="radio" placeholder="Introduce el radio" required>
        </div>
        <div>
            <button>Enviar</button>
        </div>
    </form>
</body>

</html>