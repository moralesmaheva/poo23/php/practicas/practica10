<?php
//for que nos permite introducir dos numero y al pulsar el boton nos muestre la suma
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Ejercicio 6 formulario</title>
</head>

<body>
    <form action="6salida.php" method="post">
        <div>
            <label for="numero1">Primer numero</label>
            <input type="number" name="numero1" id="numero1" placeholder="Introduce un numero" required>
        </div>
        <div>
            <label for="numero2">Segundo numero</label>
            <input type="number" name="numero2" id="numero2" placeholder="Introduce el segundo numero" required>
        </div>
        <div>
            <button>Sumar</button>
        </div>
    </form>
</body>

</html>