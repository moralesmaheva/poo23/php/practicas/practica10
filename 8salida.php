<?php
// calcular el area, perimetro y volumen de la esfera con un radio 
// enviado por formulario

//inicializar variables
$radio = 0;
$area = 0;
$longitud = 0;
$volumen = 0;

//recibimos los datos por post
$radio = $_POST["radio"];

//procesamiento
$area = pi() * pow($radio, 2);
$longitud = 2 * pi() * $radio;
$volumen = (4 / 3) * pi() * pow($radio, 3);
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Ejercicio 8 salida</title>
</head>

<body>
    <div>
        El area del circulo es <?= $area ?>
    </div>
    <br>
    <div>
        La longitud del circulo es <?= $longitud ?>
    </div>
    <br>
    <div>
        El volumen de la esfera es <?= $volumen ?>
    </div>
</body>

</html>