<?php
// indicar la suma de los numeros dados por formulario
//inicializar variables
$numero1 = 0;
$numero2 = 0;
$suma = 0;

//recibimos los datos por post
$numero1 = $_POST["numero1"];
$numero2 = $_POST["numero2"];

//procesamiento
$suma = $numero1 + $numero2;
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Ejercicio 6 salida</title>
</head>

<body>
    <!-- impresion -->
    <div>
        La suma de los numeros es <?= $suma ?>
    </div>
</body>

</html>