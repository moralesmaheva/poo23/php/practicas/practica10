<?php
// hallar la suma, resta, multiplicacion y division de los numeros dados por formulario

//inicializar variables
$numero1 = 0;
$numero2 = 0;
$suma = 0;
$resta = 0;
$producto = 0;
$division = 0;

//recibimos los datos por post
$numero1 = $_POST["numero1"];
$numero2 = $_POST["numero2"];

//procesamiento
$suma = $numero1 + $numero2;
$resta = $numero1 - $numero2;
$producto = $numero1 * $numero2;
$division = $numero1 / $numero2;
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Ejercicio 6 salida</title>
</head>

<body>
    <div>
        <?= "La suma de los numeros {$numero1} y {$numero2} es: {$suma}" ?>
    </div>
    <div>
        <?= "La resta de los numeros {$numero1} y {$numero2} es: {$resta}" ?>
    </div>
    <div>
        <?= "La multiplicacion de los numeros {$numero1} y {$numero2} es: {$producto}" ?>
    </div>
    <div>
        <?= "La division de los numeros {$numero1} y {$numero2} es: {$division}" ?>
    </div>
</body>

</html>