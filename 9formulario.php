<?php
//formulario que permite pasar una nota y nos devuelve
//0-3 muy deficiente
//3-5 suspenso
//5-6 suficiente
//6-7 bien
//7-9 notable
//9-10 sobresaliente
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Ejercicio 8 formulario</title>
</head>

<body>
    <form action="9salida.php" method="post">
        <div>
            <label for="nota">Nota</label>
            <input type="number" name="nota" id="nota" placeholder="Introduce la nota" required>
        </div>
        <div>
            <button>Enviar</button>
        </div>
    </form>
</body>

</html>