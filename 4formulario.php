<?php
//formulario que nos permite introducir un numero y al pulsar el boton nos indica si es par o impar
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Ejercicio 4 formulario</title>
</head>

<body>
    <form action="4salida.php" method="post">
        <div>
            <label for="numero">Numero:</label>
            <input type="number" name="numero" id="numero" placeholder="Introduce un numero" required>
        </div>
        <div>
            <button>Comprobar</button>
        </div>
    </form>
</body>

</html>