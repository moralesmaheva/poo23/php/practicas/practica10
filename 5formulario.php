<?php
//formulario que permite introducir tres numeros y nos indica cual es mayor
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Ejercicio 5 formulario</title>
</head>

<body>
    <form action="5salida.php" method="post">
        <div>
            <label for="numero1">Primer numero</label>
            <input type="number" name="numero1" id="numero1" placeholder="Introduce un numero" required>
        </div>
        <div>
            <label for="numero2">Segundo numero</label>
            <input type="number" name="numero2" id="numero2" placeholder="Introduce el segundo numero" required>
        </div>
        <div>
            <label for="numero3">Tercer numero</label>
            <input type="number" name="numero3" id="numero3" placeholder="Introduce el tercer numero" required>
        </div>
        <div>
            <button>Comprobar</button>
        </div>
    </form>
</body>

</html>